class SpaceShip extends GraphicObject{
 
  float damping = 0.995;
  float topspeed = 6;
  float heading = 0;
  float r = 12;
  boolean thrusting = false;
  boolean shootok=true;
  float rayon=25;

  
 SpaceShip(){}
 
 SpaceShip(PVector loc, PVector vel){
   
   this.location=loc;
   this.velocity=vel;
   this.acceleration = new PVector();
   
   
  }
  
  
  void update(float deltaTime){
    velocity.add(acceleration);
    velocity.mult(damping);
    velocity.limit(topspeed);
    location.add(velocity);
    acceleration.mult(0);
    if(deltaTime==0)
    {
      shootok=true;
    }
  }
  
  void applyForce(PVector force) {
    PVector f = force;
    acceleration.add(f);
  }

  void turn(float a) {
    heading += a;
  }
  
    void thrust() {
    float angle = heading ;
    PVector force = new PVector(cos(angle),sin(angle));
    force.mult(0.1);
    applyForce(force); 
    thrusting = true;
  }
  
  void wrapEdges() {
    float buffer = r*2;
    if (location.x > width +  buffer) location.x = -buffer;
    else if (location.x <    -buffer) location.x = width+buffer;
    if (location.y > height + buffer) location.y = -buffer;
    else if (location.y <    -buffer) location.y = height+buffer;
  }

  
  void display(){
     stroke(0);
    strokeWeight(2);
    pushMatrix();
    translate(location.x,location.y+r);
    rotate(heading+radians(90));
    fill(155,55,98);
    rect(-r/2,r,r/3,r/2);
    rect(r/2,r,r/3,r/2);

    // triangle avec shape
    beginShape();
    vertex(-r,r);
    vertex(0,-r);
    vertex(r,r);
    endShape(CLOSE);
    rectMode(CENTER);
    popMatrix();
  }
  
  
}
