class Projectile extends GraphicObject{
float rayon=15;
boolean isshoot;
float angle;
  Projectile(){}
  
  Projectile(PVector loc, PVector vel,float a){
    angle=a;
    isshoot=false;
  this.location=loc;
  this.velocity=vel;
  this.acceleration=new PVector(0,0);
  }
  
  boolean checkEdges() {
    if (location.x > width) {
      isshoot=false;
      return true;
    } 
    else if (location.x < 0) {
      isshoot=false;
      return true;
    }
 
    else if (location.y> height) {
     isshoot=false;
     return true;
    }
    
    else if(location.y-rayon<0){
     isshoot=false;
     return true;
    }
    return false;
  }
  
  void update(float delta){
  
     velocity.add(acceleration);
     location.add(velocity);
     
     acceleration.mult(0); 
  
  }

  void display(){
   
   
   fill(155,45,45);
   circle(location.x,location.y,rayon);
  
  }
}
  

  
  
