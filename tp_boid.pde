int currentTime;
int previousTime;
float deltaTime;
PVector velo;
SpaceShip ship;
boolean gameover;
ArrayList<Projectile> ammo;
int maxammo=5;
ArrayList<Mover> flock;
int flockSize = 50;
int boidhit;
float lastfire;
boolean displatext;



boolean debug = false;

void setup () {
  fullScreen(P2D);
  reset();
}

void draw () {
  
  int cpt=0;
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  previousTime = currentTime;


if (keyPressed) {
  if(key=='A' || key == 'a')
      ship.turn(-0.03);
  if(key=='g' || key == 'G')
      boidhit=50;
   if(key=='D' || key == 'd')   
      ship.turn(0.03);
   if(key=='W' || key == 'w')
      ship.thrust();   
   if(key == ' '){
    if(ammo.size()<5 && lastfire + 250<= millis() ){
      Projectile p= new Projectile(new PVector(ship.location.x,ship.location.y),new PVector(10*cos(ship.heading),10*sin(ship.heading)),ship.heading);
     lastfire=millis();
      ammo.add(p);
    deltaTime=100;
    }
  }
  
}
  for(int i=0;i<flockSize;i++){
    if(flock.get(i).contactSpaceship(ship))
    {
      gameover=true;
          
    }
    for(int j=0;j<ammo.size();j++)
    {
      if(flock.get(i).gothit==false)
        {
        if(flock.get(i).objectcontact(ammo.get(j))||ammo.get(j).checkEdges() ){
       
          if(flock.get(i).objectcontact(ammo.get(j))&&flock.get(i).gothit==true){
            boidhit++;
          
          }
        ammo.remove(j);
     
        }
  }

  }
  
  }
  update(deltaTime);
  ship.wrapEdges();
  display();  
}


void update(float delta) {
  
    if(delta>0)
    deltaTime--;
  
  println(deltaTime);  for (Mover m : flock) {
      m.flock(flock);
      m.update(delta);
    }
    for(Projectile p : ammo){
      p.update(delta);    
    }
    ship.update(delta);
  
}


void display () {
  background(0);
  
  for (Mover m : flock) {
    if(!m.gothit)
    m.display();
  }
  
   for(Projectile p : ammo){
   
      p.display();    
    }
  
  ship.display();
  
  fill(175,100,100);
  textSize(50);
  textAlign(RIGHT);
  text(boidhit+"/"+flockSize,200,50);
  
if(gameover==true && displatext==true){
  fill(255,20,20);
      textSize(100);
      textAlign(CENTER,CENTER);
      text("GAME OVER", width/2 , height/2); 
   
      
    }
 else if (boidhit>=flockSize )
 {
   fill(0,255,20);
    textSize(100);
      textAlign(CENTER,CENTER);
      text("Congratulations !", width/2 , height/2); 
      for(int i=0;i<flockSize;i++){
        flock.get(i).velocity.set(new PVector(0,0));
      }
      
   
      
 }
  
}

void keyPressed() {
  
  if(key=='R' || key == 'r')
  reset(); 
}

void reset(){

  currentTime = millis();
  previousTime = millis();
  gameover=false;
  displatext=true;
  boidhit=0;
  deltaTime=0;
  ship=new SpaceShip(new PVector(width/2,height/2),new PVector());
  lastfire=0;
  flock = new ArrayList<Mover>();
  for (int i = 0; i < flockSize; i++) {
    Mover m = new Mover(new PVector(width/6, height/6), new PVector(random (-2, 2), random(-2, 2)));
    m.fillColor = color(255);
    flock.add(m);
  }
  
  ammo= new ArrayList<Projectile>();
  
  
  flock.get(0).debug = true;


}
